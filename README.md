# [Campus Map](https://geometalab.gitlab.io/campus-maps-with-openstreetmap/ifs-campus-map/index.html) with leaflet.js

Die [Campus Map](https://geometalab.gitlab.io/campus-maps-with-openstreetmap/ifs-campus-map/index.html) ist eine interaktive, benutzbare (-> XKCD Comic) Karte des **Campus Rapperswil-Jona** der **OST**. Haupttechnologien sind Typescript und leaflet.js. Die Campus Map wurde ohne Frontend-Framework umgesetzt. Informationen zu den **Anforderungen und Funktionen** sind unter [requirements.md](docs\requirements.md) zu finden.

![XKCD University Website](https://imgs.xkcd.com/comics/university_website.png)

[source: xkcd.com](https://xkcd.com/773/)

## Events
Um ein neues Event zu event.geojson zu erstellen, um sein eigenes Event auf die Campusmap zu bringen kann das Skript [campusmap_event_converter](https://gitlab.com/geometalab/campus-maps-with-openstreetmap/campusmap_event_converter) verwendet werden. Anleitung im [README.md](https://gitlab.com/geometalab/campus-maps-with-openstreetmap/campusmap_event_converter/-/blob/master/readme.md).

Die Eventfunktionalität kann in der Datei event.ts durch Anpassung von `const EVENT` (Zeile 7) aktiviert und deaktiviert werden. Die Informationen zum Event werden als event.geojson unter ./dist/data abgelegt. Beispielformatierung:

```json
{
  "features": [
    {
      "geometry": {
        "coordinates": [8.817223, 47.223589],
        "type": "Point"
      },
      "id": "event/1",
      "properties": {
        "category": "Infomarkt",
        "description": "",
        "id": "event/1",
        "name": "Infomarkt",
        "number": "1",
        "place": "Gebäude 1",
        "time": "09.00 – 15.00",
        "url": "https://www.ost.ch/de/infoevents"
      },
      "type": "Feature"
    }
  ]
}
```
## Update
Um die Daten der Campusmap upzudaten kann man das [Script](https://gitlab.com/geometalab/campus-maps-with-openstreetmap/IFS_Campusmap_get_GeoJson_from_OSM) laufen lassen. Der inhalt des Files ost.geojson soll mit dem Inhalt des Files output.geojson überschreiben werden (Für die Campusmap muss das Script nur ausgeführt werden und die Daten übertragen werden).

## Setup

### Voraussetzungen

- [node](https://nodejs.org/en/) für die Entwicklung
- [git bash](https://gitforwindows.org/) Kenntnisse für Windowsnutzer

### Installation

- Repo herunterladen
- `npm install` im Rootfolder des Projekts ausführen. Was der Befehl macht: Er geht in die Datei `package.json` und schaut dort nach _dependencies_ und _devDependencies_ und installiert diese in einem Ordner welcher `node_modules` heisst. Der Ordner `node_modules` wird extrem gross sein. Das ist aber nicht euer Problem und ihr könnt ihn eigentlich komplett ignorieren (git ignoriert ihn auch).

### Entwicklung

- `npm run dev` - In der Entwicklung ist es praktisch, wenn man nicht jedes mal die Seite manuell neu laden muss. Deswegen gibt es den Befehl: `npm run dev`. Mit diesem Befehl wird automatisch beim Speichern eure Änderungen auf dem Browser angepasst.

### Build

- `npm run build` - In einer live Umgebung möchte man das Javascript so effizient wie möglich ausliefern. Daher wird mit dem Befehl `npm run build` die Dateien im `src/`-Ordner genommen, zusammen kompiliert und schlussendlich als `main.js` in den Ordner `dist` gelegt. Unsere `index.html`-Datei referenziert jetzt nicht mehr auf das orginal script sondern auf das neue `main.js` Script.

Probier es selber aus:

1. Lass `npm run build` laufen
2. Schau im `dist/index.html` auf die Zeile 52.
3. Gehe in das File `dist/main.js` und vergleiche es mit deinem Source Code im Ordner `src`. Siehst du Ähnlichkeiten mit deinem Orginalen Code? Nein -> Gut, weil das Javascript wurde optimiert und "production-ready" gemacht.

## Architektur

Da die Anwendung mit statischen Daten arbeitet, besteht es nur aus dem Frontend.

### Begriffe

- Feature = Ein (GeoJSON) Objekt auf der Karte/in einem Layer
- Level = Ein Stockwerk
- Layer = Eine technische Ebene der Karte wie bspw. POIs, Level Eins Korridore

### UI Naming

In der Darstellung unten siehst du, welche Bereiche des UIs wie benannt sind.

<img src="./docs/campus-map-UI naming.drawio.png" alt="campus-map-UI naming.drawio" style="width:80%;" />

### Code-Architektur

<img src="./docs/campus-map-architektur.drawio.png" alt="campus-map-architektur.drawio" style="width:80%;" />

### Ordnerstruktur

- **dist** Beinhaltet das auszuliefernde Produkt. Wenn ihr also euer Projekt ausliefern möchtet, müsst ihr die Anfragen auf die index.html Datei lenken.
- **src** Ist der Typescript Code. Der Einstiegspunkt ist hier `index.ts`.
- **package-lock.json** Könnt ihr ignorieren. Das ist eine Hilfdatei für das NPM package management.
- **package.json** Hier werden Abhängigkeiten und die beiden Commands `npm run dev` und `npm run build` definiert.
- **webpack.config.js** Das ist ein Konfigurationsfile für Webpack. Schaut mal rein.

## Was ich euch empfehlen kann zum lernen:

- (JS/Typescript)
- NPM Tutorial, [NPM](https://www.youtube.com/watch?v=2V1UUhBJ62Y)
- [LeafletJS](https://leafletjs.com/examples.html)
- Einlesen in [GeoJSON](https://leafletjs.com/examples.html)
- Bei Bedarf: Webpack basic tutorial, [WEBPACK](https://webpack.js.org/guides/getting-started/#basic-setup)

### Was ist Webpack?

Es nimmt alle Resourcen (Bilder, CSS, JS, etc.) und kompiliert sie. Somit werden die Webseiten schneller, weil weniger Code beim Aufruf der Webseite geladen werden muss. Zusätzlich hat es noch weitere Features, wie hot module reloading. Es lohnt sich jedenfalls die Doku von [Webpack](https://webpack.js.org/) zu lesen.
