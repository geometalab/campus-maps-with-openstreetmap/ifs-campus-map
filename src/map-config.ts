export const selectedFeatureStyle = {
  color: "red",
  weight: 5,
  opacity: 1,
};

export const hoverFeatureStyle = {
  color: "orange",
  weight: 5,
  opacity: 1,
};

export const roomStyle = {
  color: "#16264c",
  weight: 0,
  opacity: 0,
  fillOpacity: 1,
};

export const corridorStyle = {
  color: "gray",
  stroke: true,
  weight: 1,
  opacity: 1,
  fillOpacity: 0.5,
};
