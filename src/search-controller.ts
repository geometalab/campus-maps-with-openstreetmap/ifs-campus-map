import { EVENT } from "./event";
import { formatEventInfoBox } from "./event-view";
import { formatInfobox } from "./infobox";
import { clearSelectedFeature } from "./map-layers";
import { flyToFeature, highlightResults, resetMapView } from "./map-controls";
import {
  clearSearchModel,
  getResults,
  getSearchString,
  getSingleFeature,
} from "./search-model";
import {
  clearListbox,
  clearSearchView,
  initSearchView,
  renderListboxView,
  searchDOMReferences,
} from "./search-view";

export const createSearch = () => {
  console.log("createSearch ran");
  initSearchView();
};

export const submitSearch = () => {
  const features = getResults();
  // empty searchbox -> no features -> no results
  if (features) {
    highlightResults(features);
    if (features.length === 1) {
      selectFeature(features[0]);
    }
    clearListbox();
  }
};

export const clearSearch = () => {
  // could be better in terms of usability with or without
  //   if (getSearchString() === undefined || getSearchString().length === 0) {
  resetMapView();
  //   }
  clearSelectedFeature();
  clearSearchView();
  clearSearchModel();
};

export const searchFromQuery = async (query: string) => {
  setTimeout(() => {
    const feature = getSingleFeature(query);
    searchDOMReferences.searchInput.value = feature.properties.name;
    selectFeature(feature);
  }, 100);
};

export const listSearchResults = () => {
  const results = getResults();
  const searchInputValue = getSearchString();
  if (!searchInputValue || searchInputValue.length < 1) {
    clearListbox();
    return;
  }
  renderListboxView(results);
};

export const selectFeature = (feature: GeoJSON.Feature) => {
  console.log(feature);

  highlightResults([feature]);
  // EVENT
  if (EVENT && feature.id.toString().includes("event")) {
    formatEventInfoBox(feature);
  } else {
    formatInfobox(feature);
  }
  flyToFeature(feature);
  clearListbox();
};
