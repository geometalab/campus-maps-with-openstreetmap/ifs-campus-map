import {
  checkIfPinned,
  getEventDataPinned,
  pinEvent,
  removeAllEvents,
  unpinFeature,
} from "./event";
import { addInfoBoxClickListeners } from "./infobox";
import { searchDOMReferences } from "./search-view";

type EventDOMReferences = {
  pinboxRemoveAllButton: HTMLElement;
  pinboxShowButton: HTMLElement;
  showPinnedElement: boolean;
};

const eventDOMReferences: EventDOMReferences = {
  pinboxRemoveAllButton: undefined,
  pinboxShowButton: undefined,
  showPinnedElement: false,
};

export const renderPinnedElements = () => {
  const pinBox = document.getElementById("pinbox");

  if (getEventDataPinned().length === 0) {
    if (!pinBox.classList.contains("pinbox-hidden"))
      pinBox.classList.add("pinbox-hidden");
    return;
  }

  setupPinnedEventsBox(pinBox);

  pinBox.classList.replace("pinbox-hidden", "pinbox");

  eventDOMReferences.pinboxRemoveAllButton =
    document.getElementById("pinbox-trash");
  eventDOMReferences.pinboxShowButton = document.getElementById(
    "pinbox-accordioncontrol",
  );
  eventDOMReferences.pinboxRemoveAllButton.addEventListener("click", () => {
    removeAllEvents();
  });
  eventDOMReferences.pinboxShowButton.addEventListener("click", () =>
    togglePinnedEventsList(),
  );
};

const togglePinnedEventsList = () => {
  const element = document.getElementById("pinbox-body");

  // Check the current display style of the element
  const currentDisplayStyle = window.getComputedStyle(element).display;

  // Toggle between 'none' and 'flex' based on the current display style
  if (currentDisplayStyle === "none") {
    eventDOMReferences.showPinnedElement = true;
    element.style.display = "flex";
    eventDOMReferences.pinboxShowButton.innerText = "Ausblenden";
  } else {
    element.style.display = "none";
    eventDOMReferences.showPinnedElement = false;
    eventDOMReferences.pinboxShowButton.innerText = "Anzeigen";
  }
};

export const formatEventInfoBox = (feature: GeoJSON.Feature) => {
  const infoBox = searchDOMReferences.infoBox;
  const properties = feature.properties;

  const infotext = `
  <div class="pinbox-element-title" style="margin: 0.7em 1em;">
  <span class="event-number">${properties.number}</span>
  <span class="event-title">${properties.name}</span>
  <div class="share-button" id="share-button-${
    feature.id
  }" type="button" title="Link teilen" >
  <div id="sharePopupDiv">
    <span id="sharePopup">Copied to clipboard</span>
  </div>
  <img id="shareButtonSVG" src="data/icons/share.svg" alt="share button">
</div>

  <button id="button-pin-event-${
    properties.id
  }"><img id="" src="data/icons/pin-angle.svg" /></button>
</div> 
      ${
        properties.place === ""
          ? ""
          : `<p><b>Ort:</b> ${properties.place}</p>`
      }
      ${
        properties.time === ""
          ? ""
          : `<p><b>Zeit:</b> ${properties.time}</p>`
      }
      ${
        properties.category === ""
          ? ""
          : `<p><b>Kategorie:</b> ${properties.category}</p>`
      }
  
      ${
        properties.description === ""
        ? "" 
        : `<p><b>Beschreibung:</b> ${properties.description}</p>`    
      }
      ${
        properties.url === ""
          ? ""
          : "<p><a href=" +
            properties.url +
            ">Link zur Eventbeschreibung</a>  </p>"
      }
    `;

  infoBox.innerHTML = infotext;
  addInfoBoxClickListeners(feature);
  addInfoboxPinListener(feature);
};

export const addInfoboxPinListener = (feature: GeoJSON.Feature) => {
  const element = document.getElementById(
    `button-pin-event-${feature.properties.id}`,
  );

  // Check if the feature is already pinned and update the button state accordingly
  if (checkIfPinned(feature)) {
    element.innerHTML = `<img id="" src="data/icons/pin-angle-fill.svg" />`;
  }

  element.addEventListener("click", () => {
    if (!checkIfPinned(feature)) {
      pinEvent(feature);
      element.innerHTML = `<img id="" src="data/icons/pin-angle-fill.svg" />`;
    } else {
      unpinFeature(feature);
      element.innerHTML = `<img id="" src="data/icons/pin-angle.svg" />`;
    }
  });
};

const setupPinnedEventsBox = (pinbox: HTMLElement) => {
  const data = getEventDataPinned();
  const pinnedEvents = data.map((item) => renderPinnedEvent(item)).join("");

  const pinboxContent = `
      <div class="pinbox-header">
        <span><img id="" src="data/icons/pin-angle-fill.svg" /> Events: <b>${
          data.length
        } / 5</b></span>
        <button id="pinbox-trash">Alle entfernen</button>
        <button id="pinbox-accordioncontrol">${
          eventDOMReferences.showPinnedElement ? "Ausblenden" : "Anzeigen"
        }</button>
      </div>
      <div id="pinbox-body" class="pinbox-body" style="display: ${
        eventDOMReferences.showPinnedElement ? "flex" : "none"
      }">
      ${pinnedEvents}
      </div>
    `;
  pinbox.innerHTML = pinboxContent;

  // add UNPIN eventListener to pinned events
  data.map((item) => addPinnedEventListener(item));
};

const renderPinnedEvent = (event: GeoJSON.Feature) => {
  const properties = event.properties;
  const content = `
  <div class="pinbox-body-element">
    <div class="pinbox-element-title">
      <span class="event-number">${properties.number}</span>
      <span class="event-title">${properties.name}</span>
      <button id="event-unpin-button-${properties.id}"><img id="" width="16" height="16" src="data/icons/trash3.svg" /></button>
    </div>
  
    <p>Ort: ${properties.place}</p>
    <p>Zeit: ${properties.time}</p>
    <p>Kategorie: ${properties.category}</p>
    <p>${properties.description}</p>
    <p><a href="${properties.url}">Link zum Event</a></p>
  </div>
    `;
  return content;
};


const addPinnedEventListener = (event: GeoJSON.Feature) => {
  const element = document.getElementById(
    `event-unpin-button-${event.properties.id}`,
  );

  element.addEventListener("click", () => {
    unpinFeature(event);
  });
};
