import { loadGeoJsonData } from "./load-data";
import { getParameter } from "./helpers/params";
import { createMap } from "./map-setup";
import { createSearch, searchFromQuery } from "./search-controller";
import { createLevels } from "./level-controls";
import { createLayers } from "./map-layers";
import { createMapControls } from "./map-controls";
// import { applicationState } from "./state/application-state";

// the order is strictly defined
const setupApp = () => {
  createMap();
  createSearch();
  createLevels();
  createLayers();
  createMapControls();
};

loadGeoJsonData()
  .then(setupApp)
  .then(() => {
    // autosearch for copied/shared links
    if (getParameter("search")) {
      searchFromQuery(getParameter("search"));
    }
  });

window.onload = () => {
  const input = document.getElementById("searchInput");
  input.focus();
};
