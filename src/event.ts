import * as L from "leaflet";
import { applicationState } from "./state/application-state";
import { createPopup, selectGeojson } from "./map-layers";
import { formatEventInfoBox, renderPinnedElements } from "./event-view";
import { clearListbox } from "./search-view";

export const EVENT: boolean = false;

const loadEventData = async (): Promise<GeoJSON.FeatureCollection> => {
  return fetch("data/event.geojson").then((response) => response.json());
};

type EventDataState = {
  eventData: GeoJSON.FeatureCollection;
  eventLayer: L.GeoJSON;
  eventDataPinned: GeoJSON.Feature[];
  eventLayerPinned: L.GeoJSON;
};

const eventDataState: EventDataState = {
  eventData: undefined,
  eventLayer: undefined,
  eventDataPinned: [],
  eventLayerPinned: undefined,
};

export const bringEventsToFront = () => {
  if (eventDataState.eventLayer) eventDataState.eventLayer.bringToFront();
};

export const getEventDataPinned = () => {
  return eventDataState.eventDataPinned;
};

export const getEventData = () => {
  return eventDataState.eventData;
};

const updateEventLayerPinned = () => {
  eventDataState.eventLayerPinned.clearLayers();
  eventDataState.eventDataPinned.forEach((item) => {
    eventDataState.eventLayerPinned.addData(item);
  });
};

export const pinEvent = (feature: GeoJSON.Feature) => {
  eventDataState.eventDataPinned.push(feature);
  renderPinnedElements();
  updateEventLayerPinned();
};

export const removeAllEvents = () => {
  eventDataState.eventDataPinned = [];
  renderPinnedElements();
  updateEventLayerPinned();
};

export const unpinFeature = (feature: GeoJSON.Feature) => {
  const index = eventDataState.eventDataPinned.findIndex(
    (pinnedFeature) => pinnedFeature.properties.id === feature.properties.id,
  );
  if (index !== -1) {
    eventDataState.eventDataPinned.splice(index, 1);
    renderPinnedElements();
  }
  updateEventLayerPinned();
};

export const checkIfPinned = (feature: GeoJSON.Feature): boolean => {
  return !!eventDataState.eventDataPinned.find(
    (pinnedFeature) => pinnedFeature.properties.id === feature.properties.id,
  );
};

const geojsonMarkerOptions = {
  radius: 12,
  fillColor: "#e60071",
  color: "#000",
  stroke: false,
  weight: 1,
  opacity: 1,
  fillOpacity: 0.8,
};

const geojsonMarkerOptionsPinned = {
  radius: 15,
  fillColor: "#fff",
  color: "#fff",
  stroke: true,
  weight: 5,
  opacity: 1,
  fillOpacity: 0,
};

export const initEventData = async () => {
  if (!EVENT) {
    return;
  }
  const myMap = applicationState.map;

  eventDataState.eventData = await loadEventData();
  console.log(eventDataState.eventData);

  eventDataState.eventLayer = L.geoJSON(eventDataState.eventData, {
    pointToLayer: function (feature, latlng) {
      const label = String(feature.properties.number);
      return L.circleMarker(latlng, geojsonMarkerOptions)
        .bindTooltip(label, {
          permanent: true,
          direction: "center",
          className: "event-labels",
          opacity: 1,
        })
        .openTooltip();
    },
    onEachFeature: function (feature, layer) {
      layer.on("click", function (e) {
        L.DomEvent.stop(e);
        clearListbox();
        selectGeojson([feature]);
        formatEventInfoBox(feature);
      });
      layer.on("mouseover", () => {
        layer
          .bindTooltip(createPopup(feature), { direction: "top", opacity: 0.8 })
          .openTooltip();
      });
      layer.on("mouseout", () => {
        const label = String(feature.properties.number);
        layer
          .bindTooltip(label, {
            permanent: true,
            direction: "center",
            className: "event-labels",
            opacity: 1,
          })
          .openTooltip();
      });
    },
  }).addTo(myMap);

  eventDataState.eventLayerPinned = L.geoJSON(eventDataState.eventDataPinned, {
    pointToLayer: function (feature, latlng) {
      return L.circleMarker(latlng, geojsonMarkerOptionsPinned);
    },
    onEachFeature: function (feature, layer) {
      layer.on("click", function (e) {
        L.DomEvent.stop(e);
        clearListbox();
        selectGeojson([feature]);
        formatEventInfoBox(feature);
      });
      layer.on("mouseover", () => { });
      layer.on("mouseout", () => { });
    },
  }).addTo(myMap);
};
