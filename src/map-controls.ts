import { toggleTileServer } from "./map-setup";
import { applicationState } from "./state/application-state";
import { switchLevel } from "./level-controls";
import { selectGeojson } from "./map-layers";
import * as L from "leaflet";

export const createMapControls = () => {
  const myMap = applicationState.map;

  const plusBtn = document.querySelector("#plus");
  const minusBtn = document.querySelector("#minus");
  const changeMapBtn = document.querySelector("#changeMap");

  plusBtn.addEventListener("click", () => myMap.zoomIn());
  minusBtn.addEventListener("click", () => myMap.zoomOut());
  changeMapBtn.addEventListener("click", () => toggleTileServer());
};

export const resetMapView = () => {
  switchLevel("0");
  const myMap = applicationState.map;
  myMap.setView([47.223345, 8.817153], 18.5);
};

export const flyToFeature = (feature: GeoJSON.Feature) => {
  const myMap = applicationState.map;
  const { lat, lng } = getCenterCoordinates(feature);
  myMap.flyTo([lng, lat], 20, { animate: true, duration: 0.5 });
};

export const highlightResults = (features: GeoJSON.Feature[]) => {
  if (features.length > 0) {
    const level = features[0].properties.level;
    if (level === "-1" || level === "0" || level === "1" || level === "2") {
      switchLevel(level);
    }
  }
  selectGeojson(features);
};

export const getCenterCoordinates = (
  feature: GeoJSON.Feature,
): { lat: number; lng: number } => {
  let lat: number;
  let lng: number;
  if (feature.geometry.type === "Polygon") {
    const coordinates = feature.geometry.coordinates;
    // Braces for Desturcturing
    ({ lat, lng } = L.polygon(coordinates[0] as L.LatLngExpression[])
      .getBounds()
      .getCenter());
  } else if (feature.geometry.type === "Point") {
    const position = feature.geometry.coordinates;
    lat = position[0];
    lng = position[1];
  }
  return { lat, lng };
};
