import { EVENT, getEventData, initEventData } from "./event";
import { buildFeatureName } from "./helpers/build-feature-name";
import { applicationState } from "./state/application-state";
import { jaroWinkler } from "jaro-winkler-typescript";


type SearchState = {
  searchString: string;
  results: GeoJSON.Feature[];
};

const jaroMatch = 0.8;

const searchState: SearchState = {
  searchString: undefined,
  results: undefined,
};

export const getSearchString = () => {
  return searchState.searchString;
};

export const updateSearchstring = (searchString: string) => {
  searchState.searchString = searchString;
  updateResults();
};

export const clearSearchModel = () => {
  searchState.searchString = undefined;
  searchState.results = undefined;
};

export const getResults = () => {
  return searchState.results;
};

const updateResults = () => {
  const searchString = searchState.searchString;
  searchState.results = getAllMatchingFeatures(searchString);
};

export const getSingleFeature = (searchString: string) => {
  const results = getAllMatchingFeatures(searchString);
  if (results.length > 1) {
    const findOne = results.filter((item) => item.id === searchString);
    // some features have duplicates, thats why we take the first item after filtering
    return findOne[0];
  }
  if (results.length === 0) {
    console.error("no result");
    return;
  }
  return results[0];
};

export const getAllMatchingFeatures = (searchString: string) => {
  const geoJsonData = applicationState.geoJsonData;
  const results: { feature: GeoJSON.Feature, score: number }[] = [];

  if (searchString === "" || searchString.length < 2) {
    return [];
  }

  let filter = searchString.toLowerCase();
  const hasNum = /\d/.test(filter);

  // synonyms
  if (filter === "wc" || filter === "toilette" || filter === "toiletten") {
    filter = "toilet";
  }
  if (filter === "aufzug" || filter === "elevator") {
    filter = "lift";
  }

  for (const layerName in geoJsonData) {
    const features = geoJsonData[layerName as keyof typeof geoJsonData];
    for (const feature of features) {
      const propertyName =
        feature.properties.name === undefined
          ? ""
          : feature.properties.name.toLowerCase();
      const propertyRef =
        feature.properties.ref === undefined
          ? ""
          : feature.properties.ref.toLowerCase();
      const propertyAltName =
        feature.properties.alt_name === undefined
          ? ""
          : feature.properties.alt_name.toLowerCase();
      const propertyAmenity =
        feature.properties.amenity === undefined
          ? ""
          : feature.properties.amenity.toLowerCase();
      const buildName =
        buildFeatureName(feature.properties) === undefined
          ? ""
          : buildFeatureName(feature.properties).toLowerCase();

      const scores = [
        jaroWinkler(filter, feature.id.toString().toLowerCase()),
        jaroWinkler(filter, propertyRef),
        jaroWinkler(filter, propertyName),
        jaroWinkler(filter, propertyAltName),
        jaroWinkler(filter, propertyAmenity),
        jaroWinkler(filter, buildName)
      ];

      let maxScore = Math.max(...scores);

      if (hasNum){
        maxScore = maxScore -0.15
      }

      if ((
          propertyRef.includes(filter) ||
          propertyName.includes(filter) ||
          propertyAltName.includes(filter) ||
          feature.id.toString().includes(filter) ||
          propertyAmenity.includes(filter) ||
          buildName.includes(filter)) &&
          maxScore < jaroMatch
      ) {
        results.push({ feature, score: jaroMatch });
      } else {
        results.push({ feature, score: maxScore });
      }
    }
  }
  if (EVENT) {
    const eventData = getEventData();
    if (!eventData) initEventData();
    const features = getEventData();
    for (const feature of features.features) {
      const properties = feature.properties;
      const scores = [
        jaroWinkler(filter, properties.id.toString().toLowerCase()),
        jaroWinkler(filter, properties.name.toString().toLowerCase()),
        jaroWinkler(filter, properties.place.toString().toLowerCase()),
        jaroWinkler(filter, properties.category.toString().toLowerCase()),
        jaroWinkler(filter, properties.description.toString().toLowerCase())
      ];

      const maxScore = Math.max(...scores);

      if ((
        properties.id.toString().includes(filter) ||
        properties.name.toString().includes(filter) ||
        properties.category.toString().includes(filter) ||
        properties.description.toString().includes(filter)) &&
        maxScore < jaroMatch
      ) {
        results.push({ feature, score: jaroMatch });
      } else {
        results.push({ feature, score: maxScore });
      }
    }
  }
  return sortMatches(results);
};

const sortMatches = (matches: { feature: GeoJSON.Feature, score: number }[]) => {
  //remove undefined elements
  function removeUndefinedElements(item: { feature: GeoJSON.Feature, score: number }) {
    if (item.feature.properties.name === undefined) {
      return false;
    } else if (item.score < 0.7) {
      return false;
    } else {
      return true;
    }
  }

  matches = matches.filter(removeUndefinedElements)

  //sort bey properties.name length
  matches.sort((a, b) => {
    const aName =
        a.feature.properties.name === undefined
            ? ""
            : a.feature.properties.name.toLowerCase();
    const bName =
        b.feature.properties.name === undefined
            ? ""
            : b.feature.properties.name.toLowerCase();
    return aName.length - bName.length;
  });

  // Sort matches based on the Jaro-Winkler match score
  matches.sort((a, b) => b.score - a.score);

  const results = matches.filter(match => match.score >= jaroMatch).map(match => match.feature);

  if (results.length === 0) {
    return matches.slice(0, 5).map(match => match.feature);
  }else {
    return results;
  }
};


