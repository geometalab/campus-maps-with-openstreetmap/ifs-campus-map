import { applicationState } from "./state/application-state";

const propertiesToIgnore = [
  "natural",
  "highway",
  "emergency",
  "barrier",
  "man_made",
];

/**
 * Loads all geoJsonData to the corresponding level
 */
export const loadGeoJsonData = async () => {
  const allGeoJsonData: GeoJSON.FeatureCollection = await getData();
  const dataOnEachLevel = applicationState.geoJsonData;
  dataOnEachLevel.levelTwo = loadLevel(allGeoJsonData, "2");
  dataOnEachLevel.levelOne = loadLevel(allGeoJsonData, "1");
  dataOnEachLevel.levelZero = loadLevel(allGeoJsonData, "0");
  dataOnEachLevel.levelMinusOne = loadLevel(allGeoJsonData, "-1");

  dataOnEachLevel.levelCorridorTwo = loadLevelCorridors(allGeoJsonData, "2");
  dataOnEachLevel.levelCorridorOne = loadLevelCorridors(allGeoJsonData, "1");
  dataOnEachLevel.levelCorridorZero = loadLevelCorridors(allGeoJsonData, "0");
  dataOnEachLevel.levelCorridorMinusOne = loadLevelCorridors(
    allGeoJsonData,
    "-1",
  );

  dataOnEachLevel.POI = loadPOIs(allGeoJsonData);
};

const getData = async (): Promise<GeoJSON.FeatureCollection> => {
  return fetch("data/ost.geojson").then((response) => response.json());
};

const loadLevel = (allData: GeoJSON.FeatureCollection, levelToLoad: string) => {
  const results = [];

  for (const feature of allData.features) {
    if (feature["properties"]["level"] !== undefined) {
      const level = feature["properties"]["level"];
      const splitLevels = level.split(";");
      const indoor = feature["properties"]["indoor"];
      const room = feature["properties"]["room"];
      if (
        splitLevels.includes(levelToLoad) &&
        indoor !== "corridor" &&
        indoor !== "area"
      ) {
        results.push(feature);
      } else if (
        splitLevels.includes(levelToLoad) &&
        indoor === "area" &&
        room !== undefined
      ) {
        results.push(feature);
      } else if (
        splitLevels.includes(levelToLoad) &&
        feature["properties"]["amenity"] !== undefined
      ) {
        results.push(feature);
      }
    }
  }
  return results;
};

const loadLevelCorridors = (
  allData: GeoJSON.FeatureCollection,
  levelToLoad: string,
) => {
  const results = [];

  for (const feature of allData.features) {
    if (feature["properties"]["level"] !== undefined) {
      const level = feature["properties"]["level"];
      const splitLevels = level.split(";");
      const indoor = feature["properties"]["indoor"];
      const room = feature["properties"]["room"];
      if (
        splitLevels.includes(levelToLoad) &&
        (indoor === "corridor" || indoor === "area") &&
        room === undefined &&
        feature["properties"]["amenity"] === undefined
      ) {
        results.push(feature);
      }
    }
  }
  return results;
};

const loadPOIs = (allData: GeoJSON.FeatureCollection) => {
  const results = [];

  for (const feature of allData.features) {
    if (
      !propertiesToIgnore.every(
        (value) => feature["properties"][value] === undefined,
      )
    ) {
      continue;
    }
    if (feature["geometry"]["type"] !== "Point") {
      continue;
    }
    if (
      feature["properties"]["entrance"] !== "main" &&
      feature["properties"]["door"] !== undefined
    ) {
      continue;
    }

    // Removes Points that only contain a relation
    if (
      Object.keys(feature["properties"]).length === 2 &&
      feature["properties"]["@relations"] !== undefined
    ) {
      continue;
    }

    results.push(feature);
  }
  return results;
};
