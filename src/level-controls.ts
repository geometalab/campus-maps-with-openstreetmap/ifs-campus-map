import { selectLevel } from "./map-layers";
import { applicationState } from "./state/application-state";

type LevelControls = {
  selectLevelTwoBtn: HTMLElement;
  selectLevelOneBtn: HTMLElement;
  selecteLevelZeroBtn: HTMLElement;
  selectLevelMinusOneBtn: HTMLElement;
};

const levelControls: LevelControls = {
  selectLevelTwoBtn: undefined,
  selectLevelOneBtn: undefined,
  selecteLevelZeroBtn: undefined,
  selectLevelMinusOneBtn: undefined,
};

const initLevelControls = () => {
  levelControls.selectLevelTwoBtn = document.getElementById("selectLevelTwo");
  levelControls.selectLevelOneBtn = document.getElementById("selectLevelOne");
  levelControls.selecteLevelZeroBtn =
    document.getElementById("selectLevelZero");
  levelControls.selectLevelMinusOneBtn = document.getElementById(
    "selectLevelMinusOne",
  );
};

export const createLevels = () => {
  initLevelControls();

  levelControls.selectLevelTwoBtn.addEventListener("click", () =>
    selectLevelTwo(),
  );
  levelControls.selectLevelOneBtn.addEventListener("click", () =>
    selectLevelOne(),
  );
  levelControls.selecteLevelZeroBtn.addEventListener("click", () =>
    selectLevelZero(),
  );
  levelControls.selectLevelMinusOneBtn.addEventListener("click", () =>
    selectLevelMinusOne(),
  );

  // timeout is to ensure that corridors don't overlap e.g. stairs
  setTimeout(() => selectLevelZero(), 10);
};

export const switchLevel = (level: "-1" | "0" | "1" | "2") => {
  if (level === "2") {
    selectLevelTwo();
  } else if (level === "1") {
    selectLevelOne();
  } else if (level === "0") {
    selectLevelZero();
  } else if (level === "-1") {
    selectLevelMinusOne();
  }
};

const selectLevelTwo = () => {
  const geojsondata = applicationState.geoJsonData;
  selectButton(levelControls.selectLevelTwoBtn);
  selectLevel(
    geojsondata.levelTwo,
    geojsondata["levelCorridorTwo"],
    applicationState.map,
  );
};

const selectLevelOne = () => {
  const geojsondata = applicationState.geoJsonData;
  selectButton(levelControls.selectLevelOneBtn);
  selectLevel(
    geojsondata["levelOne"],
    geojsondata["levelCorridorOne"],
    applicationState.map,
  );
};

const selectLevelZero = () => {
  const geojsondata = applicationState.geoJsonData;
  selectButton(levelControls.selecteLevelZeroBtn);
  selectLevel(
    geojsondata["levelZero"],
    geojsondata["levelCorridorZero"],
    applicationState.map,
  );
};

const selectLevelMinusOne = () => {
  const geojsondata = applicationState.geoJsonData;
  selectButton(levelControls.selectLevelMinusOneBtn);
  selectLevel(
    geojsondata["levelMinusOne"],
    geojsondata["levelCorridorMinusOne"],
    applicationState.map,
  );
};

const selectButton = (button: HTMLElement) => {
  // unselect all level buttons first
  document
    .querySelectorAll(".levelButton")
    .forEach((element) => element.classList.remove("pressedLevelButton"));

  button.classList.add("pressedLevelButton");
};
