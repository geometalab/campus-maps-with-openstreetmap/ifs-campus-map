import * as L from "leaflet";

export type DataOnEachLevel = {
  levelTwo: GeoJSON.Feature[];
  levelOne: GeoJSON.Feature[];
  levelZero: GeoJSON.Feature[];
  levelMinusOne: GeoJSON.Feature[];
  levelCorridorTwo: GeoJSON.Feature[];
  levelCorridorOne: GeoJSON.Feature[];
  levelCorridorZero: GeoJSON.Feature[];
  levelCorridorMinusOne: GeoJSON.Feature[];
  POI: GeoJSON.Feature[];
};

const dataOnEachLevel: DataOnEachLevel = {
  levelTwo: undefined,
  levelOne: undefined,
  levelZero: undefined,
  levelMinusOne: undefined,
  levelCorridorTwo: undefined,
  levelCorridorOne: undefined,
  levelCorridorZero: undefined,
  levelCorridorMinusOne: undefined,
  POI: undefined,
};

type ApplicationState = {
  map: L.Map;
  geoJsonData: DataOnEachLevel;
};

export const applicationState: ApplicationState = {
  map: undefined,
  geoJsonData: dataOnEachLevel,
};
