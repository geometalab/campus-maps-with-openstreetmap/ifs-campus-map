import * as L from "leaflet";
import { getParameter } from "./helpers/params";
import { updateDoors, updatePOI } from "./map-layers";
import { clearSelectedFeature } from "./map-layers";
import { applicationState } from "./state/application-state";
import { clearInfobox, clearListbox } from "./search-view";

// currently no css-loader added to webpack -> added to html directly
// import "leaflet/dist/leaflet.css";

type LocalState = {
  mapTileServer: L.TileLayer;
  tileServerSatelite: boolean;
};

const localState: LocalState = {
  mapTileServer: null,
  tileServerSatelite: false,
};

export const createMap = () => {
  const { myMap, mapTileServer } = setupMap();
  applicationState.map = myMap;
  localState.mapTileServer = mapTileServer;

  myMap.on("zoomend", function () {
    updateDoors(myMap);
    updatePOI(myMap);
  });

  myMap.on("click", function () {
    clearInfobox();
    clearListbox();
    clearSelectedFeature();
  });

  myMap.on("moveend", function () {
    let address = window.location.href.split("?")[0];
    const center = myMap.getCenter();
    const zoom = myMap.getZoom();
    address += `?lat=${center.lat.toFixed(7)}&lon=${center.lng.toFixed(
      7,
    )}&zoom=${zoom}`;

    window.history.pushState("page2", "Title", address);
  });
};

const setupMap = () => {
  const myMap = loadMap();
  const mapTileServer = setMapTileServer();

  myMap.attributionControl.setPrefix(
    `Eigenen Event anzeigen lassen? <a href="https://www.ost.ch/ifs">Kontakt</a> | © <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | <a href="https://www.openstreetmap.org/fixthemap?lat=${getParameter(
      "lat",
    )}&lon=${getParameter("lon")}&zoom=18">Behebe einen Fehler</a>`,
  );
  mapTileServer.addTo(myMap);
  myMap.removeControl(myMap.zoomControl);
  myMap.options.minZoom = 17;
  return { myMap, mapTileServer };
};

const loadMap = () => {
  const lat = getParameter("lat");
  const lon = getParameter("lon");
  const zoom = getParameter("zoom");
  const mapConfig = {
    zoomSnap: 0.5,
    zoomDelta: 0.5,
    wheelPxPerZoomLevel: 60,
    wheelDebounceTime: 30,
  };

  if (lat && lon && zoom) {
    const latitude = Number(lat);
    const longitude = Number(lon);
    const zoomLevel = Number(zoom);
    return L.map("mapid", mapConfig).setView([latitude, longitude], zoomLevel);
  } else {
    return L.map("mapid", mapConfig).setView([47.223345, 8.817153], 18);
  }
};

const setMapTileServer = () => {
  return L.tileLayer(
    "https://api.maptiler.com/maps/basic/{z}/{x}/{y}.png?key=NKLz2KbROleVyHg340qT",
    {
      maxZoom: 22,
      minZoom: 17,
      maxNativeZoom: 20,
      tileSize: 512,
      zoomOffset: -1,
    },
  );
};

export const toggleTileServer = () => {
  localState.tileServerSatelite = !localState.tileServerSatelite;
  const mapTileServer = localState.mapTileServer;
  const changeMapBtn = document.getElementById("changeMap");

  if (localState.tileServerSatelite) {
    mapTileServer.setUrl(
      "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
    );
    changeMapBtn.classList.add("pressedLevelButton");
    changeMapBtn.title = "Satellitenbild aus";
  } else {
    mapTileServer.setUrl(
      "https://api.maptiler.com/maps/basic/{z}/{x}/{y}.png?key=NKLz2KbROleVyHg340qT",
    );
    changeMapBtn.classList.remove("pressedLevelButton");
    changeMapBtn.title = "Satellitenbild an";
  }
};
