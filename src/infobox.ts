import { getCenterCoordinates } from "./map-controls";
import { searchDOMReferences } from "./search-view";
import { shareLink } from "./helpers/share-link";
import { buildFeatureName } from "./helpers/build-feature-name";

/** Config formatInfobox */
const roomPropertiesToShow = [
  "name",
  "amenity",
  "ref",
  "room",
  "description",
  "indoor",
  "phone",
  "level",
  "website",
  "email",
];

export const formatInfobox = (feature: GeoJSON.Feature) => {
  const properties = feature.properties;
  const infoBox = searchDOMReferences.infoBox;
  const presentImportantProperties = Object.keys(properties).filter((key) =>
    roomPropertiesToShow.includes(key),
  );

  let infotext = buildInfoText(properties, presentImportantProperties);

  if (infotext && infotext !== "") {
    infotext += buildLinkBox(feature);
    infoBox.innerHTML = infotext;
    addInfoBoxClickListeners(feature);
  }
};

const buildParagraph = (content: string, bold?: boolean) => {
  return `<p>${bold ? "<b>" : ""}${content}${bold ? "</b>" : ""}</p>`;
};

const buildInfoText = (
  properties: GeoJSON.Feature["properties"],
  presentImportantProperties: string[],
): string => {
  // build infobox title
  const featureName = buildFeatureName(properties, false);
  let infotext = buildParagraph(featureName, true);

  // Add additional information
  // Level
  if (presentImportantProperties.includes("level")) {
    const levels = properties["level"].split(";");
    infotext +=
      levels.length === 1 || (levels.length === 2 && levels[0].startsWith("-"))
        ? buildParagraph(`Stockwerk: ${properties["level"]}`)
        : buildParagraph(
            `Stockwerke: ${levels[0]} - ${levels[levels.length - 1]}`,
          );
  }

  // Contact information
  if (presentImportantProperties.includes("website")) {
    const shortWebsite = properties["website"].replace(/^https?:\/\//i, "");
    infotext += `<p>Website: <a href="${properties["website"]}">${shortWebsite}</a></p>`;
  }

  if (presentImportantProperties.includes("phone")) {
    infotext += buildParagraph(
      `Telefonnummer: <a href="tel:${properties["phone"]}">${properties["phone"]}</a>`,
    );
  }

  if (presentImportantProperties.includes("email")) {
    infotext += `<p>E-Mail: <a href="mailto:${properties["email"]}">${properties["email"]}</a></p>`;
  }

  return infotext;
};

const buildLinkBox = (feature: GeoJSON.Feature): string => {
  const { lat, lng } = getCenterCoordinates(feature);

  return `
      <div class="linkbox">
        <p class="links">
          <a href="https://routing.osm.ch/?z=14&loc=${lng},${lat}">OSM.ch</a> |
          <a href="https://map.search.ch/${lng},${lat}">Search.ch</a> |
          <a href="https://maps.google.com/maps?q=loc:${lng},${lat}">GMaps</a>
        </p>
        <div class="share-button" id="share-button-${feature.id}" type="button" title="Link teilen" >
          <div id="sharePopupDiv">
            <span id="sharePopup">Copied to clipboard</span>
          </div>
          <img id="shareButtonSVG" src="data/icons/share.svg" alt="share button">
        </div>
      </div>`;
};

export const addInfoBoxClickListeners = (feature: GeoJSON.Feature) => {
  const element = document.getElementById(`share-button-${feature.id}`);
  element.addEventListener("click", () => {
    shareLink(feature.id.toString());
  });
};
