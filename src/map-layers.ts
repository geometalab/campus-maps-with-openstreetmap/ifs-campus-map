import * as L from "leaflet";
import { DataOnEachLevel, applicationState } from "./state/application-state";
import { formatInfobox } from "./infobox";
import { EVENT, bringEventsToFront, initEventData } from "./event";
import { formatEventInfoBox } from "./event-view";
import { clearInfobox, clearListbox } from "./search-view";
import {
  infoIcon,
  toiletIcon,
  miscellaneousIcon,
  wasteIcon,
  benchIcon,
  sculptureIcon,
  mainEntryIcon,
  fitnessIcon,
  picnicTableIcon,
  publicTransportIcon,
  doorIcon,
} from "./helpers/icons";
import {
  corridorStyle,
  hoverFeatureStyle,
  selectedFeatureStyle,
} from "./map-config";
import { buildFeatureName } from "./helpers/build-feature-name";

export const poiPropertiesToShow = [
  "name",
  "amenity",
  "locality",
  "website",
  "toursim",
  "playground",
  "operator",
  "leisure",
  "tourism",
];

const localState = {
  layerGroup: new L.LayerGroup(),
  selectedFeatureLayer: L.geoJSON(),
  selectedLevelRoomLayer: L.geoJSON(),
  selectedLevelCorridorLayer: L.geoJSON(),
  poiLayer: L.geoJSON(),
  doorsLayer: L.geoJSON(),
  selectedHoverLayer: L.geoJSON(),
};

export const createLayers = () => {
  const myMap = applicationState.map;
  const geoJsonData = applicationState.geoJsonData;
  localState.selectedHoverLayer.addTo(myMap);
  localState.layerGroup.addTo(myMap);
  localState.poiLayer = loadPoiLayer(geoJsonData);
  if (EVENT) initEventData();
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const iconMapping: { [key: string]: any } = {
  table: picnicTableIcon,
  bench: benchIcon,
  waste_basket: wasteIcon,
  picnic_table: picnicTableIcon,
  toilets: toiletIcon,
};

const loadPoiLayer = (geojsondata: DataOnEachLevel) => {
  return L.geoJSON(geojsondata["POI"], {
    pointToLayer: function (feature, latlng) {
      const props = feature.properties;

      if (props.amenity && iconMapping[props.amenity]) {
        return L.marker(latlng, {
          icon: iconMapping[props.amenity],
          interactive: false,
        });
      }

      if (props.information)
        return L.marker(latlng, { icon: infoIcon, interactive: true });
      if (props.leisure === "picnic_table")
        return L.marker(latlng, { icon: picnicTableIcon, interactive: false });
      if (props.fitness_station || props.sport === "fitness")
        return L.marker(latlng, { icon: fitnessIcon, interactive: false });
      if (props.public_transport)
        return L.marker(latlng, { icon: publicTransportIcon });
      if (props.tourism === "artwork")
        return L.marker(latlng, { icon: sculptureIcon, interactive: false });
      if (props.entrance === "main")
        return L.marker(latlng, { icon: mainEntryIcon, interactive: false });

      return L.marker(latlng, { icon: miscellaneousIcon });
    },
    onEachFeature: function (feature, layer) {
      layer.on("click", function () {
        clearInfobox();

        formatInfobox(feature);
      });
    },
  });
};

export const selectLevel = (
  results: GeoJSON.Feature[],
  rooms: GeoJSON.Feature[],
  myMap: L.Map,
) => {
  clearSelectedFeature();
  clearInfobox();
  localState.layerGroup.removeLayer(localState.selectedLevelRoomLayer);
  localState.layerGroup.removeLayer(localState.selectedLevelCorridorLayer);

  localState.selectedLevelRoomLayer = L.geoJSON(results, {
    pointToLayer: function () {
      return null;
    },
    onEachFeature: function (feature, layer) {
      layer.on("click", function (e) {
        clearListbox();

        selectGeojson([feature]);

        L.DomEvent.stop(e);
        formatInfobox(feature);
      });
      layer.on("mouseover", () => {
        drawRoomBorder(feature.geometry, hoverFeatureStyle);
        layer
          .bindTooltip(createPopup(feature), { direction: "top", opacity: 0.8 })
          .openTooltip();
      });
      layer.on("mouseout", () => {
        layer.closePopup();
        localState.layerGroup.removeLayer(localState.selectedHoverLayer);
      });
    },
  });

  localState.selectedLevelCorridorLayer = L.geoJSON(rooms, {
    pointToLayer: function () {
      return null;
    },
    onEachFeature: function (feature, layer) {
      layer.on("click", function (e) {
        clearListbox();
        selectGeojson([feature]);
        L.DomEvent.stop(e);
        formatInfobox(feature);
      });
    },
  });

  localState.selectedLevelCorridorLayer.setStyle(corridorStyle);

  localState.layerGroup.addLayer(localState.selectedLevelCorridorLayer);
  localState.layerGroup.addLayer(localState.selectedLevelRoomLayer);

  if (localState.selectedFeatureLayer !== null) {
    localState.selectedFeatureLayer.bringToFront();
  }

  if (localState.doorsLayer !== null) {
    localState.layerGroup.removeLayer(localState.doorsLayer);
  }

  const doors = [];
  for (const featureId in results) {
    if (results[featureId].geometry.type === "Point") {
      doors.push(results[featureId]);
    }
  }

  localState.doorsLayer = L.geoJSON(doors, {
    pointToLayer: function (feature, latlng) {
      if (
        feature["properties"]["door"] !== undefined &&
        feature["properties"]["entrance"] !== "main"
      ) {
        return L.marker(latlng, { icon: doorIcon, interactive: false });
      }
    },
  });
  updateDoors(myMap);

  const pressedLevelButton = document.querySelector(".pressedlvlButton");
  if (pressedLevelButton) {
    pressedLevelButton.className = "lvlButton";
  }
  localState.selectedLevelRoomLayer.bringToFront();
  // Events otherwise are behind the roomlayer
  if (EVENT) bringEventsToFront();
};

export const updateDoors = (myMap: L.Map) => {
  if (myMap.getZoom() > 20) {
    localState.layerGroup.addLayer(localState.doorsLayer);
  } else {
    localState.layerGroup.removeLayer(localState.doorsLayer);
  }
};

export const updatePOI = (myMap: L.Map) => {
  if (myMap.getZoom() > 19) {
    localState.layerGroup.addLayer(localState.poiLayer);
  } else {
    localState.layerGroup.removeLayer(localState.poiLayer);
  }
};

export const clearSelectedFeature = () => {
  localState.layerGroup.removeLayer(localState.selectedFeatureLayer);
};

export const selectGeojson = (
  geojson: GeoJSON.Feature[],
  featureStyle = selectedFeatureStyle,
) => {
  clearSelectedFeature();
  console.log(geojson);

  localState.selectedFeatureLayer = L.geoJSON(geojson, {
    style: featureStyle,
    pointToLayer: function (feature, latlng) {
      return L.circleMarker(latlng);
    },
    onEachFeature: (feature, layer) => {
      layer.on("click", (e) => {
        L.DomEvent.stop(e);
        // EVENT
        if (EVENT && feature.id.toString().includes("event")) {
          formatEventInfoBox(feature);
        } else {
          formatInfobox(feature);
        }
        clearListbox();
      });
      layer.on("mouseover", () => {
        if (EVENT && feature.id.toString().includes("event")) return;
        drawRoomBorder(feature.geometry, hoverFeatureStyle);
        layer
          .bindTooltip(createPopup(feature), { direction: "top", opacity: 0.8 })
          .openTooltip();
      });
      layer.on("mouseout", () => {
        localState.layerGroup.removeLayer(localState.selectedHoverLayer);
        layer.closeTooltip();
      });
    },
  });
  localState.layerGroup.addLayer(localState.selectedFeatureLayer);
};

const drawRoomBorder = (
  geojson: GeoJSON.Geometry,
  featureStyle = selectedFeatureStyle,
) => {
  const drawedBorderLayer = L.geoJSON(geojson, {
    style: featureStyle,
    interactive: false,
  });
  localState.layerGroup.removeLayer(localState.selectedHoverLayer);
  localState.selectedHoverLayer = drawedBorderLayer;
  localState.layerGroup.addLayer(drawedBorderLayer);
};

export const createPopup = (feature: GeoJSON.Feature) => {
  return L.tooltip().setContent(
    `<div class='popup-title'>${buildFeatureName(feature.properties)}</div>`,
  );
};
