import * as L from "leaflet";

export const infoIcon = L.icon({
  iconUrl: "data/icons/info_icon.svg",
  iconSize: [20, 20],
});

export const picnicTableIcon = L.icon({
  iconUrl: "data/icons/table-picnic.svg",
  iconSize: [20, 20],
});

export const doorIcon = L.icon({
  iconUrl: "data/icons/door.svg",
  iconSize: [40, 40],
});

export const benchIcon = L.icon({
  iconUrl: "data/icons/bench.svg",
  iconSize: [20, 20],
});

export const wasteIcon = L.icon({
  iconUrl: "data/icons/trashcan.svg",
  iconSize: [20, 20],
});

export const miscellaneousIcon = L.icon({
  iconUrl: "data/icons/marker.svg",
  iconSize: [25, 25],
});

export const toiletIcon = L.icon({
  iconUrl: "data/icons/wc.svg",
  iconSize: [20, 20],
});

export const fitnessIcon = L.icon({
  iconUrl: "data/icons/fitness.svg",
  iconSize: [20, 20],
});

export const publicTransportIcon = L.icon({
  iconUrl: "data/icons/bus.svg",
  iconSize: [20, 20],
});

export const sculptureIcon = L.icon({
  iconUrl: "data/icons/sculpture.svg",
  iconSize: [25, 25],
});

export const mainEntryIcon = L.icon({
  iconUrl: "data/icons/mainEntry.svg",
  iconSize: [40, 40],
});
