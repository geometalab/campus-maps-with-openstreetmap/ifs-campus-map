export const shareLink = (featureId: string) => {
  let address = window.location.href;
  address = removeUrlParams(["lat", "lon", "zoom", "search"], address);
  if (address.endsWith("/") || address.endsWith("index.html")) {
    address += "?search=" + featureId;
  } else {
    address += "&search=" + featureId;
  }

  // Change Url to address
  window.history.pushState("", "", address);

  // Save to clipboard
  navigator.clipboard.writeText(address).then(
    () => {
      // "Saved to clipboard" Popup when successful
      const popup = document.getElementById("sharePopup");
      popup.classList.add("showPopup");
      setTimeout(function () {
        popup.classList.remove("showPopup");
      }, 3000);
    },
    function (err) {
      console.error("Async: Could not copy text: ", err);
    },
  );
};

// Removes the parameters given in the keys array when existing
const removeUrlParams = (keys: string[], sourceURL: string) => {
  const baseURL = sourceURL.split("?")[0];
  const paramString = sourceURL.split("?")[1];
  if (paramString === undefined) {
    return sourceURL;
  }
  const params = paramString.split("&");
  const remainingParams = [];
  for (const i in params) {
    let found = false;
    for (const j in keys) {
      if (params[i].split("=")[0] === keys[j]) {
        found = true;
      }
    }
    if (!found) {
      remainingParams.push(params[i]);
    }
  }
  let remainingParamsString = "";
  if (remainingParams.length !== 0) {
    remainingParamsString += "?";
  }
  for (let i = 0; i < remainingParams.length; i++) {
    remainingParamsString += remainingParams[i];
    if (i < remainingParams.length - 1) {
      remainingParamsString += "&";
    }
  }
  return baseURL + remainingParamsString;
};
