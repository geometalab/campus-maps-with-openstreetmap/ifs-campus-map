export const langenscheidt = {
  office: "Büro",
  class: "Unterrichtszimmer",
  room: "Zimmer",
  laboratory: "Labor",
  corridor: "Korridor",
  lounge: "Lounge",
  storage: "Lagerraum",
  toilets: "Toilette",
  seminar_room: "Seminarzimmer",
  canteen: "Kantine",
  restaurant: "Restaurant",
  stairs: "Treppe",
  stairwell: "Treppenhaus",
  lobby: "Eingangshalle",
  conference: "Sitzungszimmer",
  technical: "Gebäudetechnik",
  toilet: "WC-Raum",
  computer: "Informatikraum",
  lecture: "Hörsaal",
  hall: "Halle",
  copier: "Kopierraum",
  elevator: "Lift",
  parking: "Parkhaus",
  auditorium: "Aula",
  library: "Bibliothek",
  reception: "Empfang",
  showroom: "Seminarraum",
  shower: "Dusche",
  kitchen: "Küche",
  dressing_room: "Garderobe",
  dressing: "Garderobe",
  bicycle_parking: "Fahrradständer",
  drinking_water: "Trinkwasser",
};

export const translate = (s: keyof typeof langenscheidt) => {
  return langenscheidt[s];
};

export const formatKey = (s: string) => {
  if (s === "ref") return "Raumnummer: ";
  if (s === "description") return "Beschreibung: ";
  if (s === "phone") return "Telefonnummer: ";
  if (s === "room") return "Raum: ";
  if (s === "indoor") return "Innenraum";
  if (s === "website") return "Webseite";
  if (s === "amenity") return "Ausstattung";
  if (s === "name") return "Name";
  if (s === "playground") return "Spielplatz";
  if (s === "tourism") return "Tourismus";
  if (s === "leisure") return "Freizeit";
  return s.charAt(0).toUpperCase() + s.slice(1) + ": ";
};

export const formatValue = (s: string) => {
  if (s === "laboratory") return "Labor";
  if (s === "office") return "Büro";
  if (s === "hall") return "Halle";
  if (s === "stairs") return "Treppen";
  if (s === "corridor") return "Korridor";
  if (s === "conference") return "Sitzngszimmer";
  if (s === "class") return "Klassenzimmer";
  if (s === "car_sharing") return "Carsharing";
  if (s === "bicycle_parking") return "Fahrradständer";
  if (s === "springy") return "Federwippe";
  if (s === "swing") return "Schaukel";
  if (s === "climbingframe") return "Klettergerüst";
  if (s === "drinking_water") return "Trinkwasserstelle";
  if (s === "shower") return "Dusche";
  if (s === "kitchen") return "Küche";
  if (s === "firepit") return "Feuerstelle";
  if (s === "charging_station") return "Aufladestation";
  if (s === "lecture") return "Hörsaal";
  if (s === "boat_rental") return "Bootsverleih";
  if (s === "room") return "Raum";
  if (s === "area") return "Bereich";
  if (s === "wall") return "Wand";
  if (s.startsWith("http")) return '<a href="' + s + '">Website</a>';
  return s.charAt(0).toUpperCase() + s.slice(1);
};
