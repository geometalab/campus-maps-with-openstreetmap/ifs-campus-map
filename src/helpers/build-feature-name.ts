import { langenscheidt, translate } from "./translate";

export const buildFeatureName = (
  properties: GeoJSON.Feature["properties"],
  LOG?: boolean,
) => {
  // set parameter log to true for easy debugging
  const tryTranslate = (input: keyof typeof langenscheidt) =>
    translate(input) || input;

  LOG && LOG && console.log(properties);
  LOG && console.log("name: " + properties.name);
  LOG && console.log("ref: " + properties.ref);
  LOG && console.log("room: " + properties.room);
  LOG && console.log("amentiy: " + properties.amenity);
  if (
    properties.name &&
    properties.ref &&
    !properties.name.includes(properties.ref)
  ) {
    // Institute, spezialräume
    LOG && console.log("case 1");
    return `${properties.name}${properties.ref ? ` (${properties.ref})` : ""}`;
  } else if (properties.amenity === "toilets") {
    LOG && console.log("case 2");
    return `${translate(properties.amenity)} ${properties.ref}`;
  } else if (properties.room && properties.ref) {
    LOG && console.log("case 3");
    return `${tryTranslate(properties.room)} ${properties.ref}`;
  } else if (properties.description?.startsWith("Room") && properties.ref) {
    LOG && console.log("case 4");
    return `Raum ${properties.ref}`;
  } else if (properties.description) {
    LOG && console.log("case 5");
    return properties.description;
  }
  else if (properties.room && properties.name) {
    LOG && console.log("case 6");
    return (properties.name);
  } else if (properties.room) {
    LOG && console.log("case 7");
    return tryTranslate(properties.room);
  } else if (properties.highway) {
    LOG && console.log("case 8");
    return `${translate(properties.highway)}${
      properties.ref ? ` (${properties.ref})` : ""
    }`;
  } else if (
    properties.ref ||
    properties.name ||
    properties.amenity ||
    properties.information
  ) {
    LOG && console.log("case 8");
    return (
      tryTranslate(properties.ref) ||
      tryTranslate(properties.name) ||
      tryTranslate(properties.amenity) ||
      tryTranslate(properties.information)
    );
  } else {
    LOG && console.error("case: unrecognized");
    return undefined;
  }
};
