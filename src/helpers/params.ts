export const getParameter = (key: string) => {
  const address = window.location.search;
  const parameterList = new URLSearchParams(address);
  return parameterList.get(key);
};
