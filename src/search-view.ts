import {
  getSearchString,
  getSingleFeature,
  updateSearchstring,
} from "./search-model";
import {
  clearSearch,
  listSearchResults,
  selectFeature,
  submitSearch,
} from "./search-controller";
import { buildFeatureName } from "./helpers/build-feature-name";

type SearchDOMReferences = {
  searchInput: HTMLInputElement;
  cancelButton: HTMLElement;
  infoBox: HTMLElement;
  listbox: HTMLElement;
  searchButton: HTMLElement;
};

export const searchDOMReferences: SearchDOMReferences = {
  searchInput: undefined,
  cancelButton: undefined,
  infoBox: undefined,
  listbox: undefined,
  searchButton: undefined,
};

/**
 * Creates DOM references and adds eventlisteners
 */
export const initSearchView = () => {
  initSearchDOMReferences();
  initSearchEventlisteners();
};

const initSearchDOMReferences = () => {
  searchDOMReferences.searchInput = document.querySelector("#searchInput");
  searchDOMReferences.cancelButton = document.querySelector("#cancelButton");
  searchDOMReferences.searchButton = document.querySelector("#searchButton");
  searchDOMReferences.infoBox = document.getElementById("infobox");
  searchDOMReferences.listbox = document.querySelector("#listbox");
};

const initSearchEventlisteners = () => {
  searchDOMReferences.searchInput.addEventListener("keyup", (event) => {
    event.preventDefault();
    if (event.key === "Enter") {
      submitSearch();
    } else if (event.key === "Escape") {
      clearSearch();
    } else {
      // On every new entered char, refresh results
      updateSearchstring(searchDOMReferences.searchInput.value);
      listSearchResults();
    }
  });

  searchDOMReferences.searchInput.addEventListener("focus", () => {
    // reopen searchlistbox on focus
    if (searchDOMReferences.searchInput.value.length > 0) {
      if (!getSearchString()) {
        console.log("restored searchString value");
        updateSearchstring(searchDOMReferences.searchInput.value);
      }
      listSearchResults();
    }
  });

  document.addEventListener("keydown", (event) => {
    if (event.key === "ArrowDown") {
      selectNextResult();
    } else if (event.key === "ArrowUp") {
      selectPreviousResult();
    }
  });

  searchDOMReferences.searchButton.addEventListener("click", () =>
    submitSearch(),
  );

  searchDOMReferences.cancelButton.addEventListener("click", () => {
    clearSearch();
  });
};

export const clearInfobox = () => {
  searchDOMReferences.infoBox.innerHTML = "";
};

export const clearListbox = () => {
  searchDOMReferences.listbox.innerHTML = "";
};

export const clearSearchView = () => {
  searchDOMReferences.searchInput.value = "";
  searchDOMReferences.searchInput.focus();
  clearInfobox();
  clearListbox();
};

export const renderListboxView = (results: GeoJSON.Feature[]) => {
  if (results && results.length === 0) {
    displayNoResultsMessage(getSearchString());
    return;
  }

  const searchResultsHTML = createSearchResultsHTML(results);
  searchDOMReferences.listbox.innerHTML = searchResultsHTML;
  attachSearchResultListeners();
};

const attachSearchResultListeners = () => {
  const listedSearchResults = document.querySelectorAll(".search-result");
  listedSearchResults.forEach((item) =>
    item.addEventListener("click", () => {
      const feature = getSingleFeature(item.getAttribute("data-id"));
      console.log(item.getAttribute("data-id"));

      selectFeature(feature);
    }),
  );
};

const displayNoResultsMessage = (searchInput: string) => {
  searchDOMReferences.listbox.innerHTML = `
    <h3>Leider ergab Ihre Suche keine Treffer. <br>
      <a href="https://gitlab.com/geometalab/campus-maps-with-openstreetmap/ifs-campus-map/-/wikis/Hilfe">Hilfe</a> |
      <a href="mailto:rj-geometalab@ost.ch?subject=Feedback%20zur%20Suche%20in%20IFS%20Campus%20Map&body=Liebes%20IFS%20/%20Geometa%20Lab%20%0A%0AFeedback%20zur%20IFS%20Campis%20Map:%20Ich%20suchte%20nach%20${searchInput}.%20%0A%0ALiebe%20Grüsse,">
        Suchanfrage mailen
      </a>
    </h3>`;
};

const createSearchResultsHTML = (results: GeoJSON.Feature[]): string => {
  let innerHTML = '<ul class="search-results">';
  innerHTML += results
    .map((item) => {
      const displayName = item.id.toString().includes("event")
        ? item.properties.name
        : buildFeatureName(item.properties);
      return `<button 
          tabindex="1" 
          class="search-result" 
          data-id=${item.id}>
            ${displayName}
        </button>`;
    })
    .join(" ");
  innerHTML += "</ul>";
  return innerHTML;
};

const selectNextResult = () => {
  const results = document.getElementsByClassName("search-result");
  const focusedResult = getFocusedResult();
  if (results.length < focusedResult + 2) {
    // If at last result, wrap back around to first result
    focusResult(0);
  } else if (results.length > focusedResult + 1) {
    focusResult(focusedResult + 1);
  }
};

const selectPreviousResult = () => {
  const focusedResult = getFocusedResult() * 1;
  if (focusedResult === 0) {
    // If at first result, wrap back around to last result
    const results = document.getElementsByClassName("search-result");
    focusResult(results.length - 1);
  } else if (focusedResult > 0) {
    focusResult(focusedResult - 1);
  }
};

// for select prev/next
const getFocusedResult = (): number => {
  const results = document.querySelectorAll(".search-result");
  let selectedProperty = undefined;
  for (const i in results) {
    console.log(i);

    if (results[i] === document.activeElement) {
      selectedProperty = i;
    }
  }
  return Number(selectedProperty);
};

// Visual focusing
const focusResult = (resultNr: number) => {
  if (resultNr < 0) {
    return;
  }
  document
    .querySelector<HTMLInputElement>(
      `.search-result:nth-child(${resultNr + 1})`,
    )
    .focus();
};
