/* eslint-disable */

const path = require('path'); // importing some packages. Be aware we are here in the nodejs environment

const config = {
  entry: './src/index.ts', // the entry file, which we will compile
  output: {
    filename: 'main.js', // name of the compiled JS code file
    path: path.resolve(__dirname, 'dist'), // destination folder after build is done
  },
  devServer: {
      static: './dist', // base for hot module reloading
      // hot: true // we enable hot module reloading
  },

};

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.devtool = 'source-map';
    // config.devtool = 'inline-source-map';
  }

  config.module = {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  }

  config.resolve = {
    extensions: ['.tsx', '.ts', '.js'],
  }

  return config
};