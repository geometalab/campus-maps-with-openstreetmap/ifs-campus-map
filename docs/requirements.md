# Requirements

## User Stories

- Als Benutzer möchte ich auf einer **interaktiven Karte** das gesamte  Campusgelände sehen können, um einen **Überblick** über den Standort und die Layouts der Gebäude zu erhalten.
  - Als Benutzer möchte ich auf der Karte **Gebäudeebenen** auswählen können, um die Räume und Einrichtungen auf den verschiedenen Etagen zu sehen.
  - Als Benutzer möchte ich auf der Karte auf bestimmte **Räume** klicken  können, um spezifische Informationen wie Raumnummer, Raumtyp und evtl. Belegungsplan zu erhalten.
  - Als Benutzer möchte ich die **Öffnungszeiten und Kontaktdetails** von POIs  wie der Cafeteria, Bibliothek, Laboren, usw. sehen können.
- Als Benutzer möchte ich die Möglichkeit haben, die Karte auf verschiedene Arten zu zoomen und zu navigieren (z.B. Drehen, Schwenken).
  - Die Karte soll Mobil als auch am Desktop gut bedienbar sein.
- Als Benutzer möchte ich eine **Suchfunktion** nutzen können, um nach Raumnummern und Points of Interest (POI) wie Cafeteria, Bibliothek, Laboren und dergleichen zu suchen.
  - Es sollen **Vorschläge** gemacht werden: WCs, und andere Spezialkategorien
- **Informationen** auf der Karte
  - Als Benutzer möchte ich Notdienststandorte wie Erste-Hilfe-Stationen und Feuerlöscher auf der Karte sehen können, um in Notfällen vorbereitet zu sein.
  - Als Benutzer möchte ich die Standorte von Fahrradabstellplätzen,  Parkplätzen und Bushaltestellen sehen können, um meine Reise zu und von dem Campus zu planen.
  - Recyclingstationen
  - Toiletten
  - Aufenthaltsbereiche
- **Link sharing**: Als Benutzer möchte ich die Möglichkeit haben, Links zu spezifischen Räumen und POIs zu kopieren und zu teilen, um Freunden, Kollegen oder Besuchern Anweisungen zu geben oder sie zu einem bestimmten Ort auf dem Campus zu führen.
  - Ein Raum oder mehrere Räume

#### Ausbau Events

- Pinned Objects: Als Benutzer möchte ich verschiedene Räume oder andere Objekte der Karte anpinnen können, damit ich mehrere unterschiedliche Punkte versenden oder Drucken kann



## Features

- :black_square_button: **Interaktive Kartenanzeige:** Anzeige des gesamten Campusgeländes mit Gebäuden, Straßen, Grünflächen und wichtigen Wahrzeichen.
  - Informationen
    - ☑️ Raumnummer, Raumtyp 
    - Institute
    - Drucker
    - Erste-Hilfe-Stationen, AED und Feuerlöscher
    - Recyclingstationen, Toiletten, Aufenthaltsbereiche
    - Fahrradabstellplätze, Parkplätze, Bahnhof und Bushaltestellen.
  - Funktionen
    - ☑️ Zoom- und Navigationssteuerung
    - ☑️ Mehrere Stockwerkebenen
    - **Informationsanzeige für POIs:** Anzeige von relevanten Informationen zu POIs, wie Links, Öffnungszeiten und Kontaktdetails.
- ☑️ **Suchfunktion:** Eine eingebettete Suchleiste, die die Suche nach Raumnummern und Points of Interest (POI) ermöglicht.
- :black_square_button: **Suchvorschläge**: Unterhalb der Suchleiste sollen Vorschläge für nützliche Informationen gemacht werden, die ausgewählt werden können und dann auf der Karte markiert werden und im Suchfeld eingefügt.
- ☑️ **Link-Sharing-Funktion:** Möglichkeit zum Kopieren und Teilen von Links zu spezifischen Räumen und POIs.
  - Beim Aufrufen des Links wird der Titel in das Suchfeld gefüllt, die 
- ☑️ **Responsive Design:** Das Design der Karte sollte auf verschiedenen Geräten, darunter Desktop, Tablet und Handy, gut funktionieren und ansprechend aussehen.

#### Optional

- **Analytics**: Speicherung Sucheingaben

- User location

- **Personalisierung**: Speicherung zuletzt gesucht
