# Events

Diese Funktion wurde für den Tag der offenen Tür implementiert.

Die Implementation kann via der Konstanten 'EVENT' in [./src/event.ts](./src/event.ts) de-/aktiviert werden.

### Funktionen

- Anzeige der Events
- Eigene Infobox
- Pin-Funktion: Events lassen sich anpinnen (und wieder entfernen)
- Printer-View: Die Druckansicht ist mit CSS so optimiert, dass nur die Karte und gepinnte Events gefunden werden.



### Datenstruktur

Alle Events haben eine id die mit 'event/' beginnt.

``````json
{
  "geometry": {
    "coordinates": [8.817292370317, 47.2235748426],
    "type": "Point"
  },
  "id": "event/78",
  "properties": {
    "category": "Vorführung",
    "description": "",
    "id": "event/78",
    "name": "Mit KI in neue Welten",
    "number": "78",
    "place": "Gebäude 1 Foyer Nord",
    "time": "permanent",
    "url": "https://events.ost.ch/tag-der-offenen-tuer-in-rapperswil/eventplaner-tag-der-offenen-tuer-in-rapperswil/veranstaltungen/ai-photobooth"
  },
  "type": "Feature"
},
``````

